#!/usr/bin/env bash

PROG=$( basename "$0" )
VP="$HOME/tmp/void-packages"
build=""
verbose=""

TEMP=$( getopt --options bihv --longoptions build,install,help,verbose -- "$@" ) || exit 1
eval set -- "$TEMP"

for i in "$@"; do
    case "$i" in
        -b|--build)
            build="set"
            shift
            ;;
        -i|--install)
            build="set"
            shift
            ;;
        -v|--verbose)
            verbose="set"
            shift
            ;;
        -h|--help|*)
            echo "Usage: $PROG OPTIONS [targets ...]"
            echo
            echo "Install template in $VP"
            echo
            echo "OPTIONS"
            echo "-b|--build         invoke build as well"
            echo "-i|--install       invoke install after build (implies --build)"
            echo "-v|--verbose       be verbose"
            exit 0
            ;;
    esac
done
shift # --

[[ "$pkg" ]] || exit

for pkg in "$@"; do

    [[ "$verbose" ]] && set -x

    [[ -d "$VP" ]] || {
        echo "$PROG: no such directory '$VP'" >&2
        exit 1
    }
    
    mkdir -p "$VP/srcpkgs/$pkg"

    cp -f "$pkg/template" "$VP/srcpkgs/$pkg/"
    # sub-packages:
    sed -n 's/\([^#[:space:]]*\)_package() .*/\1/p' "$pkg/template" | while read -r p; do
        echo ln -sf "$VP/$pkg" "$p"
    done
        
    [[ "$build$install" ]] && {
        ./xbps-src pkg "$pkg" && {
            [[ "$install" ]] && sudo xbps-install -R hostdir/binpkgs "$pkg"
        }
    }
done
