## Name
void-templates

## Description
Some templates to build out-of-repo packages for voidlinux.

## Usage

./install.sh src.template

... assumes you have void-packages checked out in ~/tmp and bootstrapped

## License
GPL-3

## Project status
All working including mythtv